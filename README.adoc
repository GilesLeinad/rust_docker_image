= Readme

Docker image including `clippy` and `fmt` which are not installed by default.
Providing https://nexte.st/[cargo-nextest] as test runner.

== Building the image:

Log in to the registry on GitLab:
----
docker login registry.gitlab.com
----

Build the image with the same version tag as the base version used (e.g. _1.78.0_).
----
docker build -t registry.gitlab.com/gilesleinad/rust_docker_image:1.78.0 .
----

Push the image to to GitLab:
----
docker push registry.gitlab.com/gilesleinad/rust_docker_image:1.78.0
----

== Using the image

To use the image in the CI/CD pipeline add following line to the `.gitlab-ci.yml`:
----
image: registry.gitlab.com/gilesleinad/rust_docker_image:1.78.0
----