FROM rust:1.78.0

RUN rustup component add rustfmt &&\
    rustup component add clippy &&\
    apt-get -y update &&\
    apt-get install -y  libudev-dev &&\
    curl -LsSf https://get.nexte.st/latest/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin
